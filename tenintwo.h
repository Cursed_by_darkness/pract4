#ifndef TENINTWO_H
#define TENINTWO_H
#include <QString>

QString TenInTwo(QString val);

QString TenInEight(QString val);

QString TenInSixteen(QString val);

/**
*\file
*\brief Заголовочный файл функций перевода из 10-ной в 2-ную, 8-ную, 16-ную систему счисления
*\author Alexandr
*\version 1.4
*/

#endif // TENINTWO_H
