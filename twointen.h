#ifndef TWOINTEN_H
#define TWOINTEN_H
#include <QString>

QString TwoInTen (QString val);

/**
*\file
*\brief Заголовочный файл функции перевода из 2-ной в 10-ную систему счисления
*\author Alexandr
*\version 1.4
*/

#endif // TWOINTEN_H
