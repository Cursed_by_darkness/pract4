#include "tenintwo.h"
#include <QString>

QString EightInTwo(QString val)
{
    QString str;
    QString result;
    QString val1 = nullptr;
    for (int i = 0; i < val.length(); ++i)
    {
         str = TenInTwo(val.at(i));
         for (int i = str.size()-1, j=0; i >= 0; i--, j++)
         {
             val1[j] = str[i];
         }
         while ((val1.length() != 3) && (val1 != nullptr))
         {
             val1 = val1 + "0";
         }
         str = "";
         for (int i = val1.size()-1, j=0; i >= 0; i--, j++)
         {
             str[j] = val1[i];
         }
         result += str;
         val1 = "";
    }
    while (result.at(0) == '0')
    {
        result.remove(0,1);
    }
    return result;
}

/**
*\file
*\brief Описание функции перевода из 8-ной в 2-ную систему счисления
*\author Alexandr
*\version 1.4
*/
