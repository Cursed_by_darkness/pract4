#include "tenintwo.h"
#include "twoineight.h"
#include <QString>

QString SixteenInEight(QString val)
{
    QString str;
    QString val1;
    QString result;
    for (int i = 0; i < val.length(); ++i)
    {
        if (!val.at(i).isNumber())
        {
            if (val.at(i) == "A"){
                str = TenInTwo("10");
            }
            if (val.at(i) == "B"){
                str = TenInTwo("11");
            }
            if (val.at(i) == "C"){
                str = TenInTwo("12");
            }
            if (val.at(i) == "D"){
                str = TenInTwo("13");
            }
            if (val.at(i) == "E"){
                str = TenInTwo("14");
            }
            if (val.at(i) == "F"){
                str = TenInTwo("15");
            }
        }
        else
        {
             str = TenInTwo(val.at(i));

        }
        for (int i = str.size()-1, j=0; i >= 0; i--, j++)
        {
            val1[j] = str[i];
        }
        while (val1.length() != 4)
        {
            val1 = val1 + "0";
        }
        str = "";
        for (int i = val1.size()-1, j=0; i >= 0; i--, j++)
        {
            str[j] = val1[i];
        }
        result += str;
        val1 = "";
    }
    result = TwoInEight(result);
    return result;
}

/**
*\file
*\brief Описание функции перевода из 16-ной системы счисления в 8-ную
*\author Alexandr
*\version 1.4
*/
