#include "math.h"
#include <QString>

/**
*\file
*\brief Описание функции перевода из 2-ной в 10-ную систему счисления
*\author Alexandr
*\version 1.4
\code
QString TwoInTen (QString val)
{
    int count = 0;
    int result = 0;
    for (int i = val.length()-1; i >= 0; --i)
    {
        if (val.at(i) == '1')
        {
            result += pow(2,count);
        }
        count++;
    }
    val.setNum(result);
    return val;
}
\endcode
*/
QString TwoInTen (QString val)
{
    int count = 0;
    int result = 0;
    for (int i = val.length()-1; i >= 0; --i)
    {
        if (val.at(i) == '1')
        {
            result += pow(2,count);
        }
        count++;
    }
    val.setNum(result);
    return val;
}


