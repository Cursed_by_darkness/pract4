QT += testlib
QT -= gui

TARGET = tst_pract3_tests
CONFIG += console testcase ##qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES +=  \
    tst_pract3_tests.cpp \
    main.cpp

SOURCES += \
    ../eightinsixteen.cpp \
    ../eightintwo.cpp \
    ../sixteenineight.cpp \
    ../tenintwo.cpp \
    ../twoineight.cpp \
    ../twointen.cpp

HEADERS += \
    tst_pract3_tests.h

HEADERS += \
    ../eightinsixteen.h \
    ../eightintwo.h \
    ../sixteenineight.h \
    ../tenintwo.h \
    ../twoineight.h \
    ../twointen.h
