/****************************************************************************
** Meta object code from reading C++ file 'tst_pract3_tests.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../tst_pract3_tests.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'tst_pract3_tests.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Pract3_Tests_t {
    QByteArrayData data[10];
    char stringdata0[148];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Pract3_Tests_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Pract3_Tests_t qt_meta_stringdata_Pract3_Tests = {
    {
QT_MOC_LITERAL(0, 0, 12), // "Pract3_Tests"
QT_MOC_LITERAL(1, 13, 19), // "test_eightinsixteen"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 15), // "test_eightintwo"
QT_MOC_LITERAL(4, 50, 19), // "test_sixteenineight"
QT_MOC_LITERAL(5, 70, 13), // "test_tenintwo"
QT_MOC_LITERAL(6, 84, 15), // "test_twoineight"
QT_MOC_LITERAL(7, 100, 13), // "test_twointen"
QT_MOC_LITERAL(8, 114, 15), // "test_tenineight"
QT_MOC_LITERAL(9, 130, 17) // "test_teninsixteen"

    },
    "Pract3_Tests\0test_eightinsixteen\0\0"
    "test_eightintwo\0test_sixteenineight\0"
    "test_tenintwo\0test_twoineight\0"
    "test_twointen\0test_tenineight\0"
    "test_teninsixteen"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Pract3_Tests[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x08 /* Private */,
       3,    0,   55,    2, 0x08 /* Private */,
       4,    0,   56,    2, 0x08 /* Private */,
       5,    0,   57,    2, 0x08 /* Private */,
       6,    0,   58,    2, 0x08 /* Private */,
       7,    0,   59,    2, 0x08 /* Private */,
       8,    0,   60,    2, 0x08 /* Private */,
       9,    0,   61,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Pract3_Tests::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Pract3_Tests *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->test_eightinsixteen(); break;
        case 1: _t->test_eightintwo(); break;
        case 2: _t->test_sixteenineight(); break;
        case 3: _t->test_tenintwo(); break;
        case 4: _t->test_twoineight(); break;
        case 5: _t->test_twointen(); break;
        case 6: _t->test_tenineight(); break;
        case 7: _t->test_teninsixteen(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Pract3_Tests::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_Pract3_Tests.data,
    qt_meta_data_Pract3_Tests,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Pract3_Tests::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Pract3_Tests::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Pract3_Tests.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Pract3_Tests::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
