#ifndef TST_PRACT3_TESTS_H
#define TST_PRACT3_TESTS_H

#include <QtCore>
#include <QtTest/QtTest>

class Pract3_Tests : public QObject
{
    Q_OBJECT

public:
    Pract3_Tests();

private slots:
    void test_eightinsixteen();
    void test_eightintwo();
    void test_sixteenineight();
    void test_tenintwo();
    void test_twoineight();
    void test_twointen();
    void test_tenineight();
    void test_teninsixteen();
};

#endif // TST_PRACT3_TESTS_H
