#include "eightintwo.h"
#include "twointen.h"
#include "tenintwo.h"
#include <QString>

QString EightInSixteen(QString val)
{
    val = EightInTwo(val);
    val = TwoInTen(val);
    val = TenInSixteen(val);
    return val;
}

/**
*\file
*\brief Описание функции перевода из 8-ной в 16-ную систему счисления
*\author Alexandr
*\version 1.4
*/
